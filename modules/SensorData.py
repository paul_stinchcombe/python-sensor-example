#!/usr/bin/python3

import traceback
import serial
import time
import json
import os
import threading
import asyncio

try:
    from modules.Stack import Stack
except Exception:
    from Stack import Stack




class SensorData(threading.Thread): 
    _exitFlag = False
    _sd = None
    current = None


    def toHex(self, b):
        val = hex(b)
        if len(val) < 4:
            vals = val.split('0x')
            val = '0x0' + vals[1]
        return val


    def printHexPacket(self, packet):
        for b in packet:
            print(f'{self.toHex(b)}', end=' ')
        print()



    def getCurrent(self):   
        return self.current



    def _go(self, device):
        print(f'Thread starting on \'{device}\'')        

        ser = None
        try:
            ser=serial.Serial(device)
        except Exception:
            print(f"Unable to open serial port '{device}'")
            SensorData._exitFlag = True

        time.sleep(1)

        while not SensorData._exitFlag:
            try:
                # Read the data stream until we detect a packet 'end' byte (0x15 in this case)
                packet = ser.read_until(b'\x15')
                self.printHexPacket(packet)

                # process the byte stream to a dictionary
                hex_data = hex(packet)
                json_string = json.dumps({'data': hex_data})

                # call the registered callback with the new data
                if not self.callback == None:
                    asyncio.run(self.callback(json_string))


            except KeyboardInterrupt: 
                exitFlag = true
                break

            except Exception as e: 
                print(f"Unable to open serial port '{device}'")
                SensorData._exitFlag = True

        try: 
            ser.close()
        except:
            pass

    def getDevice(self):
        return self.serialDevice


    def __init__(self, serialDevice, callback=None, debug=False):
        threading.Thread.__init__(self)
        self.serialDevice = serialDevice
        self.callback = callback
        self.debug = debug
        self.current = self


    def run(self):
      self._go(self.serialDevice)
    

    def stop(self):
        if self._sd == None or self._exitFlag:
            return False

        self._exitFlag = True
        self._sd.join()
        print("SensorData thread has completed")
        return True



## Test for this class ##
if __name__ == '__main__':
    # Test callback method
    async def cb(j):
        print(j)

    # Do the test on serial port 1
    sd = SensorData('/dev/ttyACM0', cb, False).start()

    # Run for 10 seconds
    timeStarted = int(time.time())
    while(int(time.time())-timeStarted < 10):
        pass

    sd.stop()
    sd._sd.join()
    print('Execution complete!')
