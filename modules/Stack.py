class Stack:

    def __init__(self):
        self._stack = []

    def push(self, dataval):
        self._stack.append(dataval)
        return True

    def size(self):
        return len(self._stack)

    def peek(self): 
        return self._stack[-1]
        
    def pop(self, dummy=0):
        if len(self._stack) <= 0:
            return ("No element in the Stack")
        else:
            return self._stack.pop()
