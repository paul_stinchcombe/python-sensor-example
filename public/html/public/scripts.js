const ioUrl = window.location.protocol+'//'+window.location.host;
const socket = io(ioUrl);
try {                
    socket.on("connect", ()=>{
        console.log("Connected");
        //window.reload();
    });

    socket.on("data", (json)=>{
        let data = JSON.parse(json);        
        console.log(data);

        let style = `class = "border-success" style='color: green';` 
        document.getElementById('data').innerHTML = `<h2 ${style}>${json}</h2>`;
    });
} catch(err) {
    document.getElementById('data').innerHTML = err;
}
