aiohttp==3.7.3
aioify==0.3.2
async-timeout==3.0.1
pyserial==3.5
python-engineio==4.0.0
python-socketio==5.0.4
RPi.GPIO==0.7.0
tomlkit==0.5.11
typing-extensions==3.7.4.3
yarl==1.6.3
