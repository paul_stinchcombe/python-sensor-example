#!/usr/bin/python3

from aiohttp import web
import aioify
import socketio
import json
import time
import threading

from modules.SensorData import SensorData  



class Web():
    PORT = 8081;
    Sensors = None
    LastEventTime = time.time()
    EVENT_REFRESH_TIME = 1.0    # Every 1 second

    sio = socketio.AsyncServer()


    # Send the data to the html
    async def onSensorData(self, jsonData):
        try:
            data = json.loads(jsonData)
        except Exception as e: 
            print(e) 
            return   

        sensorIndex = self.indexForSensorDevice(data['device'])

        if time.time() - Web.LastEventTime >= Web.EVENT_REFRESH_TIME:               
            datadict = { 'sensor': sensorIndex, 'timestamp': int(time.time()*1000), 'data': data }
            await Web.sio.emit('data', json.dumps(datadict, indent=4, sort_keys=True))
            Web.lastEventTime = time.time()


    def indexForSensorDevice(self, device):
        for i, s in enumerate(Web.Sensors):
            if s.getDevice() == device:
                return i
        return 0


    def __init__(self):
        # Initialise the web socket and http server        
        self.app = web.Application()
        Web.sio.attach(self.app)

        self.sensors = [
            SensorData('/dev/ttyACM0', self.onSensorData, False),
            SensorData('/dev/ttyACM1', self.onSensorData, False)
        ]
        for s in self.sensors: 
            s.start()
        Web.Sensors = self.sensors


    async def index(self, request):
        with open('public/html/index.html') as f:
            return web.Response(text=f.read(), content_type='text/html')


    @sio.event
    async def connect(self, sid, environ=None, auth=None):
        print(f"Socket connected")


    @sio.event
    async def disconnect(self):
        print(f"Socket disconnected")

    
    @sio.on('message')
    async def print_message(sid, message):
        ## When we receive a new event of type
        ## 'message' through a socket.io connection
        ## we print the socket ID and the message
        print("Socket ID: ", sid)
        print(message)



    def start(self): 
        self.app.add_routes([web.static('/public', 'public/html/public', show_index=True)])
        self.app.router.add_get('/', self.index)

        web.run_app(self.app, port=Web.PORT)



## We kick off our server
if __name__ == '__main__':    
    ws = Web()
    ws.start()
